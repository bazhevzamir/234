// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActorBonus.h"
#include "SnakeActor.h"
#include "Engine/World.h"

// Sets default values
AMyActorBonus::AMyActorBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    bIsInteracted = false;
}

// Called when the game starts or when spawned
void AMyActorBonus::BeginPlay()
{
	Super::BeginPlay();
    GetWorldTimerManager().SetTimer(InactivityTimerHandle, this, &AMyActorBonus::DestroyIfInactive, 5.0f, false);
}

void AMyActorBonus::DestroyIfInactive()
{
    if (!bIsInteracted)
    {
        Destroy(); // ���������� ������, ���� �� ���� ��������������
    }
}

// Called every frame
void AMyActorBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMyActorBonus::Interact(AActor* Interactor, bool bIsHead)
{
    if (bIsHead)
    {
        auto Snake = Cast<ASnakeActor>(Interactor);
        if (IsValid(Snake))
        {
            Snake->AddSnakeElement();
            Snake->SetActorTickInterval(0.2f);

            FTimerHandle TimerHandle;
            GetWorldTimerManager().SetTimer(TimerHandle, [Snake]()
            {
                    Snake->SetActorTickInterval(0.5f);
            }, 5.0f, false);
           
            Destroy();
            Snake->Speedscore++;
        }

    }
}

