// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActorWall.h"
#include "SnakeActor.h"
#include "Engine/World.h"

// Sets default values
AMyActorWall::AMyActorWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyActorWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyActorWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMyActorWall::Interact(AActor* Interactor, bool bIsHead)
{
    if (bIsHead)
    {
        auto Snake = Cast<ASnakeActor>(Interactor);
        if (IsValid(Snake))
        {
           Snake->Destroy();
        }

    }
}

