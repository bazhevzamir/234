// Fill out your copyright notice in the Description page of Project Settings.


#include "MyDobleBonus.h"
#include "SnakeActor.h"
#include "Engine/World.h"

// Sets default values
AMyDobleBonus::AMyDobleBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bIsInteracted = false;
}

// Called when the game starts or when spawned
void AMyDobleBonus::BeginPlay()
{
	Super::BeginPlay();
	GetWorldTimerManager().SetTimer(InactivityTimerHandle, this, &AMyDobleBonus::DestroyIfInactive, 5.0f, false);
}

void AMyDobleBonus::DestroyIfInactive()
{
	if (!bIsInteracted)
	{
		Destroy(); // ���������� ������, ���� �� ���� ��������������
	}
}

// Called every frame
void AMyDobleBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMyDobleBonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeActor>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Destroy();
			Snake->Doublescore++;
		}
	}
}

