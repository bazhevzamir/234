// Fill out your copyright notice in the Description page of Project Settings.


#include "MyFoodActor.h"
#include "SnakeActor.h"
#include "Engine/World.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AMyFoodActor::AMyFoodActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;



}

// Called when the game starts or when spawned
void AMyFoodActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyFoodActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMyFoodActor::Interact(AActor* Interactor, bool bIsHead)
{
    if (bIsHead)
    {
        auto Snake = Cast<ASnakeActor>(Interactor);
        if (IsValid(Snake))
        {
            Snake->AddSnakeElement();
            Destroy();
            Snake->score++;
        }

    }
}


	


