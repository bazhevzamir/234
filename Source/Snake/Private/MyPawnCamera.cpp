
#include "MyPawnCamera.h"
#include "Camera/CameraComponent.h"
#include "SnakeActor.h"
#include "Components/InputComponent.h"
#include "MyActorBonus.h"
#include "MyFoodActor.h"
#include "MyDobleBonus.h"


AMyPawnCamera::AMyPawnCamera()
{

	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}


void AMyPawnCamera::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
}


void AMyPawnCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	BuferTime += DeltaTime;
	if (BuferTime > StepDeley)
	{
		AddSpawnRandomFood();
	}
	if (BuferTime > StepDeleyDoble)
	{
		AddSpawnRandomBonusDoble();
	}	
	if (BuferTime > StepDeleySpeed)
	{
		AddSpawnRandomBonusSpeed();
	}
}



void AMyPawnCamera::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &AMyPawnCamera::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &AMyPawnCamera::HandlePlayerHorizontalInput);
}

void AMyPawnCamera::CreatSnakeActor()
{
	if (SnakeActorClass)
	{
		SnakeActor = GetWorld()->SpawnActor<ASnakeActor>(SnakeActorClass, FTransform());
		GeamMode = 1;
	}
}

void AMyPawnCamera::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void AMyPawnCamera::HandlePlayerHorizontalInput(float value)
{
	
		if (IsValid(SnakeActor))
		{
			if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
			{
				SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
			}
			else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
			{
				SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
			}
		}
}

void AMyPawnCamera::AddSpawnRandomFood()
{
	if (CanSpawnMoreFood())
	{
		FRotator StartPointRotation = FRotator(0, 0, 0);
		float SpawnX = FMath::FRandRange(MinX, MaxX);
		float SpawnY = FMath::FRandRange(MinY, MaxY);
		float SpawnZ = FMath::FRandRange(MinZ, MaxZ);
		FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);
		

		if (GetWorld())
		{
			FActorSpawnParameters SpawnParams;
			AMyFoodActor* NewFoodActor = GetWorld()->SpawnActor<AMyFoodActor>(FoodActorClass, StartPoint, StartPointRotation, SpawnParams);
			if (NewFoodActor)
			{
				NewFoodActor->SetActorLocation(StartPoint);
			}
		}
	}
}

bool AMyPawnCamera::CanSpawnMoreFood()
{
	int32 MaxFoodCount = 2; 
	int32 ExistingFoodCount = 0;
	UWorld* World = GetWorld();
	if (World)
	{
		for (TActorIterator<AMyFoodActor> ActorItr(World); ActorItr; ++ActorItr)
		{
			ExistingFoodCount++;
		}
	}
	return ExistingFoodCount < MaxFoodCount;
}
void AMyPawnCamera::AddSpawnRandomBonusDoble()
{
	if (CanSpawnMoreBonusSpeed())
	{
		FRotator StartPointRotation = FRotator(0, 0, 0);
		float SpawnX = FMath::FRandRange(MinX, MaxX);
		float SpawnY = FMath::FRandRange(MinY, MaxY);
		float SpawnZ = FMath::FRandRange(MinZ, MaxZ);
		FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);
		

		if (GetWorld())
		{
			FActorSpawnParameters SpawnParams;
			AMyDobleBonus* NewDobleBonus = GetWorld()->SpawnActor<AMyDobleBonus>(DobleBonusClass, StartPoint, StartPointRotation, SpawnParams);
			if (NewDobleBonus)
			{
				NewDobleBonus->SetActorLocation(StartPoint);
			}
		}
	}
}

bool AMyPawnCamera::CanSpawnMoreBonusDuble()
{
	int32 MaxDobleBonusCount = 1;
	int32 ExistingDobleBonusCount = 0;
	UWorld* World = GetWorld();
	if (World)
	{
		for (TActorIterator<AMyDobleBonus> ActorItr(World); ActorItr; ++ActorItr)
		{
			ExistingDobleBonusCount++;
		}
	}
	return ExistingDobleBonusCount < MaxDobleBonusCount;
}

void AMyPawnCamera::AddSpawnRandomBonusSpeed()
{
	if (CanSpawnMoreBonusSpeed())
	{
		FRotator StartPointRotation = FRotator(0, 0, 0);
		float SpawnX = FMath::FRandRange(MinX, MaxX);
		float SpawnY = FMath::FRandRange(MinY, MaxY);
		float SpawnZ = FMath::FRandRange(MinZ, MaxZ);
		FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);
		

		if (GetWorld())
		{
			FActorSpawnParameters SpawnParams;
			AMyActorBonus* NewActorBonus = GetWorld()->SpawnActor<AMyActorBonus>(ActorBonusClass, StartPoint, StartPointRotation, SpawnParams);
			if (NewActorBonus)
			{
				NewActorBonus->SetActorLocation(StartPoint);
			}
		}
	}
}

bool AMyPawnCamera::CanSpawnMoreBonusSpeed()
{
	int32 MaxActorBonusCount = 1;
	int32 ExistingActorBonusCount = 0;
	UWorld* World = GetWorld();
	if (World)
	{
		for (TActorIterator<AMyActorBonus> ActorItr(World); ActorItr; ++ActorItr)
		{
			ExistingActorBonusCount++;
		}
	}
	return ExistingActorBonusCount < MaxActorBonusCount;
}

int32 AMyPawnCamera::GetScore()
{
	if (SnakeActor)
	{
		return SnakeActor->score;
	}
	return  0;
}

int32 AMyPawnCamera::GetSpeedScore()
{
	if (SnakeActor)
	{
		return SnakeActor->Speedscore;
	}
	return  0;
}

int32 AMyPawnCamera::GetDoubleScore()
{
	if (SnakeActor)
	{
		return SnakeActor->Doublescore;
	}
	return  0;
}








