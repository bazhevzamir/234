
#include "SnakeActor.h"
#include "Interacteble.h"
#include "SnakeElementsBase.h"

ASnakeActor::ASnakeActor()
{
    PrimaryActorTick.bCanEverTick = true;
    ElementSize = 100.f;
    LastMoveDirection = EMovementDirection::DOWN;
   float BaseMovementSpeed = 10.f;
}

void ASnakeActor::BeginPlay()
{
    Super::BeginPlay();
    SetActorTickInterval(MovementSpeed);
    AddSnakeElement(5);
}

void ASnakeActor::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    Move();
    void SetMiddleElementType();
    void SetLastElementType();
    void SetFirstElementType();
}

void ASnakeActor::AddSnakeElement(int ElementsNum)
{
    int32 LastIndex = SnakeElements.Num() - 1;

    for (int i = 0; i < ElementsNum; ++i)
    {
        FVector NewLocation((LastIndex + i + 1) * ElementSize, 0, 0); 
        FTransform NewTransform(NewLocation);
        ASnakeElementsBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementsBase>(SnakeElementClass, NewTransform);
        NewSnakeElem->SnakeOwner = this;
        int32 ElemIndex = SnakeElements.Add(NewSnakeElem);

        if (ElemIndex == 0)
        {
            NewSnakeElem->SetFirstElementType(); 
        }
        else if (ElemIndex == LastIndex + 1)
        {
            NewSnakeElem->SetLastElementType();
        }
        else if (ElemIndex !=0 && ElemIndex != LastIndex)
        {
            NewSnakeElem->SetMiddleElementType();
        }

        NewSnakeElem->SetActorHiddenInGame(true);
        FTimerHandle TimerHandle;
        GetWorldTimerManager().SetTimer(TimerHandle, [NewSnakeElem]()
        {
            NewSnakeElem->SetActorHiddenInGame(false);
        }, 0.9f, false);
    }
}


void ASnakeActor::SetElementVisibilityDelayed(ASnakeElementsBase* Element, bool bIsVisible, float Delay)
{
    FTimerHandle TimerHandle;
    GetWorldTimerManager().SetTimer(TimerHandle, [Element, bIsVisible]()
    {
        Element->SetActorHiddenInGame(bIsVisible);
    }, Delay, false);
}

void ASnakeActor::Move()
{
    FVector MovementVector = FVector::ZeroVector;

    switch (LastMoveDirection)
    {
    case EMovementDirection::UP:
        MovementVector.X = +ElementSize;
        break;
    case EMovementDirection::DOWN:
        MovementVector.X = -ElementSize;
        break;
    case EMovementDirection::LEFT:
        MovementVector.Y = -ElementSize;
        break;
    case EMovementDirection::RIGHT:
        MovementVector.Y = +ElementSize;
        break;
    }


    SnakeElements[0]->ToggleCollision();


    for (int i = SnakeElements.Num() - 1; i > 0; i--)
    {
        auto CurrentElement = SnakeElements[i];
        auto PrevElement = SnakeElements[i - 1];
        FVector PrevLocation = PrevElement->GetActorLocation();
        CurrentElement->SetActorLocation(PrevLocation);
    }

    SnakeElements[0]->AddActorWorldOffset(MovementVector);
    SnakeElements[0]->ToggleCollision();
}

void ASnakeActor::SnakeElementOverlap(ASnakeElementsBase* OverlappedElement, AActor* Other)
{
    if (IsValid(OverlappedElement))
    {
        int32 ElemIndex;
        SnakeElements.Find(OverlappedElement, ElemIndex);
        bool bIsFirst = ElemIndex == 0;
        Iinteracteble* InteractebleInterface = Cast<Iinteracteble>(Other);
        if (InteractebleInterface)
        {
            InteractebleInterface->Interact(this, bIsFirst);
        }
    }
}










