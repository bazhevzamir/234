
#include "SnakeElementsBase.h"
#include "SnakeActor.h"
#include "Components/StaticMeshComponent.h"

ASnakeElementsBase::ASnakeElementsBase()
{
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

void ASnakeElementsBase::BeginPlay()
{
	Super::BeginPlay();

}

void ASnakeElementsBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElementsBase::SetFirstElementType_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementsBase::HandleBeginOverlap);
}

void ASnakeElementsBase::SetLastElementType_Implementation()
{

}

void ASnakeElementsBase::SetMiddleElementType_Implementation()
{
}

void ASnakeElementsBase::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeActor>(Interactor);
	if (IsValid(Snake))
	{
		Snake->Destroy();
	}
}

void ASnakeElementsBase::HandleBeginOverlap(UPrimitiveComponent* OverLappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
	}
}

void ASnakeElementsBase::ToggleCollision()
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}



