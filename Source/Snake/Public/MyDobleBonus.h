// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interacteble.h"
#include "MyDobleBonus.generated.h"

UCLASS()
class SNAKE_API AMyDobleBonus : public AActor, public Iinteracteble
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyDobleBonus();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void DestroyIfInactive();

	FTimerHandle InactivityTimerHandle;
	bool bIsInteracted; // ���� ��� ������������ �������������� � ��������

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	virtual void Interact(AActor* Interactor, bool bIsHead)override;

};
