
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MyActorBonus.h"
#include "MyFoodActor.h"
#include "MyDobleBonus.h"
#include "MyGameModeBase.h"
#include "EngineUtils.h"
#include "MyPawnCamera.generated.h"

class  UCameraComponent;
class  ASnakeActor;
class  AMyFoodActor;//
class  AMyDobleBonus;//
class  AMyActorBonus;//

UCLASS()
class SNAKE_API AMyPawnCamera : public APawn
{
	GENERATED_BODY()

public:
	
	AMyPawnCamera();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AMyFoodActor>FoodActorClass;//

	UPROPERTY()
	TArray<AMyFoodActor*>FoodActor;//
	/// <summary>
	/// /
	/// </summary>
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AMyDobleBonus>DobleBonusClass;//

	UPROPERTY()
	TArray<AMyDobleBonus*>DobleBonus;//
	/// <summary>
	/// /
	/// </summary>
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AMyActorBonus>ActorBonusClass;//

	UPROPERTY()
	TArray<AMyActorBonus*>ActorBonus;//

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ASnakeActor* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeActor> SnakeActorClass;

protected:
	
	virtual void BeginPlay() override;

public:
	
	virtual void Tick(float DeltaTime) override;

	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
	void CreatSnakeActor();

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);

	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);
	//���
	void AddSpawnRandomFood();
	bool CanSpawnMoreFood();
	//������� ���
	void AddSpawnRandomBonusDoble();
	bool CanSpawnMoreBonusDuble();
	//��������
	void AddSpawnRandomBonusSpeed();
	bool CanSpawnMoreBonusSpeed();

	float MinX = -1800.f;    float MaxX = 1800.f;
	float MinY = -2000.f;    float MaxY = 2000.f;
	float MinZ =  500.f;     float MaxZ = 2000.f;

	float StepDeley = 3.f; // �������� ��� Food
	float StepDeleyDoble = 10.f; // �������� ��� BonusDoble 
	float StepDeleySpeed = 5.f; // �������� ��� BonusSpeed 
	float BuferTime = 0;

	bool GamePause = false;

	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
	bool GetGamePause() const{ return GamePause; }

	int32 GeamMode=0;
	UFUNCTION(BlueprintCallable,Category ="SnakePawn")
	int32 GetGeamMode()const { return GeamMode; }

	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
	int32 GetScore();
	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
	int32 GetSpeedScore();
	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
	int32 GetDoubleScore();


};

