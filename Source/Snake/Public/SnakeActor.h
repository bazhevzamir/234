// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TimerManager.h"
#include "SnakeActor.generated.h"

class ASnakeElementsBase;


UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKE_API ASnakeActor : public AActor
{
	GENERATED_BODY()

public:
	ASnakeActor();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementsBase>SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY()
	TArray<ASnakeElementsBase*>SnakeElements;

	UPROPERTY();
	EMovementDirection LastMoveDirection;

protected:

	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;


	void AddSnakeElement(int ElememntsNum = 1);
	void Move();
	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementsBase* OverlappedElement, AActor* Other);

	void AddSnakeElement(int ElementsNum, bool bIsVisible);

	void SetElementVisibilityDelayed(ASnakeElementsBase* Element, bool bIsVisible, float Delay);


	FTimerHandle VisibilityTimerHandle; 

	int32 score = 0;
	int32 Speedscore = 0;
	int32 Doublescore = 0;
};


