
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interacteble.h"

#include "SnakeElementsBase.generated.h"


class UStaticMeshComponent;
class ASnakeActor;

UCLASS()
class SNAKE_API ASnakeElementsBase : public AActor, public Iinteracteble
{
    GENERATED_BODY()

public:
    ASnakeElementsBase();

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    UStaticMeshComponent* MeshComponent;

    UPROPERTY()
    ASnakeActor* SnakeOwner;

protected:
    virtual void BeginPlay() override;

public:
    virtual void Tick(float DeltaTime) override;

    UFUNCTION(BlueprintNativeEvent)
    void SetFirstElementType();
    void SetFirstElementType_Implementation();

    UFUNCTION(BlueprintNativeEvent)
    void SetLastElementType();
    void SetLastElementType_Implementation();

    UFUNCTION(BlueprintNativeEvent)
    void SetMiddleElementType();
    void SetMiddleElementType_Implementation();

    virtual void Interact(AActor* Interactor, bool bIsHead) override;

    UFUNCTION()
    void HandleBeginOverlap(UPrimitiveComponent* OverLappedComponent,
        AActor* OtherActor,
        UPrimitiveComponent* OtherComp,
        int32 OtherBodyIndex,
        bool bFromSweep,
        const FHitResult& SweepResult);

    UFUNCTION()
    void ToggleCollision();
};


